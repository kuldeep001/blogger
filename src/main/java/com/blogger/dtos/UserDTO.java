package com.blogger.dtos;

import lombok.Data;

@Data
public class UserDTO {
   private String name;
   private String emailAddress;
   private String contactNo;
}
