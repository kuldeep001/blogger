package com.blogger.dtos;

import lombok.Data;

@Data
public class BlogDTO {

   private int userId;
   private String blogTitle;
   private String blogContent;


}
