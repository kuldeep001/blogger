package com.blogger.services;


import com.blogger.domainmodels.User;

import com.blogger.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserService {

   private final UserRepository userRepository;

   public String createUser(User user) {
      if (isExistingUser(user)) {
         return "User Already Exist with similar Email Address.";
      }
      User save = userRepository.save(user);
      log.info("User Created with User={}", save);
      return "User created successfully with userId = " + save.getUserId();
   }

   public User getUserById(int userId) {
      Optional<User> userOptional = userRepository.findById(userId);
      if (userOptional.isPresent()) {
         return userOptional.get();
      }
      return null;
   }

   boolean isExistingUser(User user) {
      return null != userRepository.findByEmailAddress(user.getEmailAddress());
   }
}
