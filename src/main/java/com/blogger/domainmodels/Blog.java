package com.blogger.domainmodels;

import lombok.Data;

import java.util.List;

@Data
public class Blog {

   private int blogId;
   private int userId;
   private String blogTitle;
   private String blogContents;
   private List<Comment> commentList;
   private int likes;

}
