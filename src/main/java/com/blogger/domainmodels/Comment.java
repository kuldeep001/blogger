package com.blogger.domainmodels;


import lombok.Data;

@Data
public class Comment {

   private int blogId;
   private String userId;
   private String commentText;

}
