package com.blogger.domainmodels;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Getter
@Setter
@Builder
@Entity
public class User {

   @Id
   @GeneratedValue(strategy= GenerationType.SEQUENCE)
   private int userId;
   private String name;
   @Column(name="emailAddress", unique=true)
   private String emailAddress;
   private String contactNo;


}
