package com.blogger.controllers;


import com.blogger.domainmodels.User;
import com.blogger.dtos.UserDTO;
import com.blogger.services.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/blogger")
@RequiredArgsConstructor
public class UserController {

   private final UserService userService;

   @GetMapping("/blog/{blogId}")
   public String getBlog(@PathVariable("blogId") String blogId) {

      return "this is blog id=" + blogId;
   }

   @PostMapping("/user")
   public void createUser(@RequestBody UserDTO userDTO) {
      User user = User.builder().name(userDTO.getName())
         .emailAddress(userDTO.getEmailAddress())
         .contactNo(userDTO.getContactNo())
         .build();
      userService.createUser(user);

   }
}
