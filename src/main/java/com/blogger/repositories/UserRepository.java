package com.blogger.repositories;

import com.blogger.domainmodels.User;
import org.springframework.data.repository.CrudRepository;



public interface UserRepository extends CrudRepository<User, Integer> {

   User findByEmailAddress(String emailAddress);

}
